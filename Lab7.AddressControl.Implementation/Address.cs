﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab7.AddressControl
{
    public class Address : IAddress
    {
        private Control control = new AdressCon();
        public Control Control
        {
            get { return control; }
        }

        public event EventHandler<AddressChangedArgs> AddressChanged;
    }
}
